//
//  MyTaskListTests.swift
//  MyTaskListTests
//
//  Created by RJ Balandra on 07/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import XCTest
@testable import MyTaskList

class MyTaskListTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testIntervalComputation() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let ownTask = OwnTask()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/y"
        //dateFormatter.date(from: "01/11/2018")
        let days = ownTask.getInterval(date: "11/01/2018")
        
        XCTAssertTrue(days == 13)
        
    }
    
    func testRemarkValue() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let ownTask = OwnTask()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let remark = ownTask.getRemark(date: "01/11/2018")
        
        XCTAssertFalse(remark == .onboard)
        
    }
    

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
