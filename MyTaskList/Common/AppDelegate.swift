//
//  AppDelegate.swift
//  MyTaskList
//
//  Created by RJ Balandra on 07/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import SwinjectStoryboard
import Swinject
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {

    var window: UIWindow?
    let container: Assembler = Assembler()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        Container.loggingFunction = nil  // disable since doesn't work with storyboard plugin
        container.apply(assemblies: [NetworkAssembly(), AppAssembly()])
//        container.apply(assembly: AppAssembly())

        // SETUP ROOT VIEW CONTROLLER
        let storyboard = SwinjectStoryboard.create(name: "Main", bundle: nil, container: container.resolver)
        let initialViewController = storyboard.instantiateInitialViewController()!
        window!.rootViewController = initialViewController
        window!.makeKeyAndVisible()

        //Sample for conditional targets for changing staging and production values
        #if TEST
        print("We have a TEST")
        #else
        print("We don't have a TEST")
        
        #endif
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) { }

    func applicationDidEnterBackground(_ application: UIApplication) { }

    func applicationWillEnterForeground(_ application: UIApplication) { }

    func applicationDidBecomeActive(_ application: UIApplication) { }

    func applicationWillTerminate(_ application: UIApplication) { }

}
