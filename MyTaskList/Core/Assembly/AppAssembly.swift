//
//  AppAssembly.swift
//  MyTaskList
//
//  Created by RJ Balandra on 07/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift
import Swinject
import SwinjectStoryboard
import UIKit

class AppAssembly: Assembly {

    func assemble(container: Container) {

        container.register(JokeListPresenter.self, factory: { (resolver)  in
            let presenter = JokeListPresenterImplementation()
            presenter.realm = resolver.resolve(Realm.self)
            presenter.disposeBag = resolver.resolve(DisposeBag.self)
            presenter.gateway = resolver.resolve(APIGateway.self)
            return presenter
        })
        
        container.register(MenuPresenter.self, factory: { (resolver)  in
            let presenter = MenuPresenterImplementation()
            presenter.realm = resolver.resolve(Realm.self)
            presenter.disposeBag = resolver.resolve(DisposeBag.self)
            //presenter.gateway = resolver.resolve(APIGateway.self)
            return presenter
        })

        container.register(TaskListPresenter.self, factory: { (resolver)  in
            let presenter = TaskListPresenterImplementation()
            presenter.realm = resolver.resolve(Realm.self)
            presenter.disposeBag = resolver.resolve(DisposeBag.self)
            presenter.gateway = resolver.resolve(APIGateway.self)
            return presenter
        })

        container.register(AddTaskPresenter.self, factory: { (resolver)  in
            let presenter = AddTaskPresenterImplementation()
            presenter.realm = resolver.resolve(Realm.self)
            presenter.disposeBag = resolver.resolve(DisposeBag.self)
            return presenter
        })

        container.register(DetailViewPresenter.self, factory: { (resolver)  in
            let presenter = DetailViewPresenterImplementation()
            presenter.realm = resolver.resolve(Realm.self)
            presenter.disposeBag = resolver.resolve(DisposeBag.self)
            return presenter
        })

        // MARK: View Controllers
        container.storyboardInitCompleted(MenuTableViewController.self) { resolver, controller in
            controller.presenter = resolver.resolve(MenuPresenter.self)
        }
        
        container.storyboardInitCompleted(AddTaskViewController.self) { resolver, controller in
            controller.realm = resolver.resolve(Realm.self)
            controller.presenter = resolver.resolve(AddTaskPresenter.self)
            controller.disposeBag = resolver.resolve(DisposeBag.self)
        }

        container.storyboardInitCompleted(TaskListViewController.self) { resolver, controller in
            controller.realm = resolver.resolve(Realm.self)
            controller.presenter = resolver.resolve(TaskListPresenter.self)
            controller.disposeBag = resolver.resolve(DisposeBag.self)
        }

        container.storyboardInitCompleted(JokeListViewController.self) { resolver, controller in
            controller.presenter = resolver.resolve(JokeListPresenter.self)
        }
        
        container.storyboardInitCompleted(DetailViewController.self) { resolver, controller in
            controller.disposeBag = resolver.resolve(DisposeBag.self)
            controller.presenter = resolver.resolve(DetailViewPresenter.self)
            controller.realm = resolver.resolve(Realm.self)
        }

        container.storyboardInitCompleted(UserProfileViewController.self) { resolver,controller in
            controller.userDefaults = resolver.resolve(UserDefaults.self)
        }
        // MARK: Realm
        // swiftlint:disable force_try
        container.register(Realm.self) { _ in
            return try! Realm()
        }
        
        container.register(DisposeBag.self, factory: { _ in
            return DisposeBag()
        })

        container.register(UserDefaults.self, factory: { _ in
            return UserDefaults.standard
        })
        // swiftlint:enable force_try

    }

}
