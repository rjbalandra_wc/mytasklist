//
//  NetworkAssembly.swift
//  MyTaskList
//
//  Created by RJ Balandra on 08/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import Foundation
import Swinject
import SwinjectStoryboard
import UIKit

class NetworkAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.register(APIGateway.self, factory: { _ in APIGatewayImplementation() })
        
    }
    
}
