//
//  TaskRemarks.swift
//  MyTaskList
//
//  Created by RJ Balandra on 14/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import Foundation

enum TaskRemarks {
    
    case accomplished
    case ongoing
    case onboard
    case stopped
    
    var description: String {
        switch self {
        case .accomplished: return "accomplished"
        case .ongoing: return "ongoing"
        case .onboard: return "onboard"
        case .stopped: return "stopped"
        }
    }
}
