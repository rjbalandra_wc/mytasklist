//
//  Menu.swift
//  MyTaskList
//
//  Created by RJ Balandra on 09/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

enum Menu: Int {
    case tasks = 0
    case jokes = 1
    case profile = 2

    static var count: Int { return Menu.profile.rawValue + 1 }
    
    var description: String {
        switch self {
        case .tasks: return "My Tasks"
        case .jokes  : return "Chuck Norris Jokes"
        case .profile: return "User Profile"
        }
    }
    
    var segueIdentifier: String {
        switch self {
        case .tasks: return "showTaskList"
        case .jokes  : return "showJokes"
        case .profile: return "showUserProfile"
        }
    }
    
    init?(value: Int) {
        switch value {
        case 0 : self = .tasks
        case 1 : self = .jokes
        case 2 : self = .profile
        default : return nil
        }
    }
}
