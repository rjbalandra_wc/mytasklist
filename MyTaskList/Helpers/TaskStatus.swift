//
//  Status.swift
//  MyTaskList
//
//  Created by RJ Balandra on 08/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

enum TaskStatus: Int {
    case done = 0
    case progress = 1
    case not = 2
    static var count: Int { return TaskStatus.not.rawValue + 1 }
    
    var description: String {
        switch self {
        case .done: return "Done"
        case .progress  : return "In Progress"
        case .not : return "Not Started"
        }
    }
    
    init?(value: Int) {
        switch value {
        case 0 : self = .done
        case 1 : self = .progress
        case 2 : self = .not
        default : return nil
        }
    }
}
