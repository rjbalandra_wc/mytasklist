//
//  Date+ISO8601.swift
//  MyTaskList
//
//  Created by RJ Balandra on 14/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import Foundation

extension Date {
    init?(ISO8601: String) {
        let isoFormatter = ISO8601DateFormatter()
        
        guard let date = isoFormatter.date(from: ISO8601) else { return nil }
        self = date
        print(date)
    }
}
