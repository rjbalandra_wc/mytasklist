//
//  MenuPresenter.swift
//  MyTaskList
//
//  Created by RJ Balandra on 09/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift

protocol MenuPresenter: class {
    var numberOfItems: Int { get }
    func viewDidLoad()
    func attachView(view: MenuView)
    func configureWithData(cell: TaskListTableViewCell, for data: OwnTask)
    func didSelectWithData(menu: Menu)
    func addTaskToRealm(task: OwnTask)
}

class MenuPresenterImplementation: MenuPresenter {

    var realm: Realm!
    var disposeBag: DisposeBag?
    var view: MenuView?

    var numberOfItems: Int {
        do {
            return Menu.count
        }
    }

    func viewDidLoad() {
        
    }
    
    func attachView(view: MenuView) {
        self.view = view
    }
    
    func configureWithData(cell: TaskListTableViewCell, for data: OwnTask) {
        
    }
    
    func didSelectWithData(menu: Menu) {
        view?.showDetails(for: menu.segueIdentifier)
    }
    
    func addTaskToRealm(task: OwnTask) {
        
    }

}
