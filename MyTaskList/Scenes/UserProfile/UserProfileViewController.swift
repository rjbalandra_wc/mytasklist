//
//  UserProfileViewController.swift
//  MyTaskList
//
//  Created by RJ Balandra on 14/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import Foundation
import UIKit

class UserProfileViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var userDefaults: UserDefaults?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let editButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(updateObject(_:)))
        navigationItem.rightBarButtonItem = editButton
        
        if let decoded  = userDefaults?.object(forKey: "profile") {
            let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded as! Data) as! Profile
            nameTextField.text = decodedTeams.name
            descriptionTextView.text = decodedTeams.profileDescription
        }
      
    }
    
    @objc
    func updateObject(_ sender: Any) {

        let profile = Profile()
        profile.name = nameTextField.text ?? ""
        profile.profileDescription = descriptionTextView.text ?? ""

        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: profile)
        userDefaults?.set(encodedData, forKey: "profile")
        userDefaults?.synchronize()
        
        performSegue(withIdentifier: "goBackToMenu", sender: self)
    }

}
