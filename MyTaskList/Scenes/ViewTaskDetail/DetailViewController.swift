//
//  DetailViewController.swift
//  MyTaskList
//
//  Created by RJ Balandra on 07/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift

protocol DetailViewControllerDelegate: class {
    func updateModel(model: OwnTask)
}

protocol DetailView: class {
    func updateLabel(title: String)
    func updateLabel(description: String)
    func updateLabel(date: String)
    func updateLabel(status: String)
}

class DetailViewController: UIViewController, DetailView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!

    var realm: Realm!
    var presenter: DetailViewPresenter?
    var disposeBag: DisposeBag?
    
    var detailItem: OwnTask?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let editButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(updateObject(_:)))
        navigationItem.rightBarButtonItem = editButton

        if let detail = detailItem {
            presenter?.setTask(task: detail)
            presenter?.attachView(view: self)
            presenter?.viewDidLoad()
        }
    }
    
    // MARK: update labels
    func updateLabel(title: String) {
        self.titleLabel.text = title
    }
    
    func updateLabel(description: String) {
        self.descriptionLabel.text = description
    }
    
    func updateLabel(date: String) {
        self.dateLabel.text = date
    }
    
    func updateLabel(status: String) {
        self.statusLabel.text = status
    }
    
    @objc
    func updateObject(_ sender: Any) {
        performSegue(withIdentifier: "updateTask", sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination.isKind(of: AddTaskViewController.self) {

                let controller = segue.destination as! AddTaskViewController
                controller.isTaskEditing = true
                controller.task = detailItem
                controller.delegate = self
        }
    }

}

extension DetailViewController: DetailViewControllerDelegate {
    func updateModel(model: OwnTask) {
        self.detailItem = model
        presenter?.setTask(task: model)
        presenter?.viewDidLoad()
    }
}
