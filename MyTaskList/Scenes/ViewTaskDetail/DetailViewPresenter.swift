//
//  DetailViewPresenter.swift
//  MyTaskList
//
//  Created by RJ Balandra on 12/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift

protocol DetailViewPresenter: class {
    func viewDidLoad()
    func attachView(view: DetailView)
    func setTask(task: OwnTask)
}

class DetailViewPresenterImplementation: DetailViewPresenter {
    
    var realm: Realm!
    var view: DetailView?
    var disposeBag: DisposeBag?
    var task: OwnTask?

    func viewDidLoad() {
        
        if let status = task?.getRemark(date: task?.date ?? "") {
            view?.updateLabel(status: status.description)
        }
        
        view?.updateLabel(date: task?.date ?? "")
        view?.updateLabel(title: task?.title ?? "")
        view?.updateLabel(description: task?.taskDescription ?? "")
    }
    
    func attachView(view: DetailView) {
        self.view = view
    }

    func setTask(task: OwnTask) {
        self.task = task
    }

}
