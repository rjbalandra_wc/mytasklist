//
//  AddTaskViewController.swift
//  MyTaskList
//
//  Created by RJ Balandra on 07/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift
import RxCocoa

private let minimalUsernameLength = 5
private let minimalPasswordLength = 5

protocol AddTaskView: class {
    func showPrompt()
    func showUpdatePrompt()
    func didSelectPicker(status: String)
}

class AddTaskViewController: UIViewController, AddTaskView {
 
    @IBOutlet weak var taskTitleTextField: UITextField!
    @IBOutlet weak var taskDescriptionTextView: UITextView!
    @IBOutlet weak var taskDateTextField: UITextField!
    @IBOutlet weak var taskStatusTextField: UITextField!
    
    var realm: Realm!
    var presenter: AddTaskPresenter?
    var disposeBag: DisposeBag?
    var isTaskEditing: Bool = false
    var editedTask: OwnTask? 
    
    let datePicker = UIDatePicker()
    let statusPicker = UIPickerView()
    
    weak var delegate: DetailViewControllerDelegate?

    var task: OwnTask? {
        didSet {
            
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        presenter?.attachView(view: self)
        presenter?.setupObservables(completionHandler: { _ in })
    
        setupValidators()
        setupDatePicker()
        setupStatusPicker()
        
       configureView()
    }
    
    func configureView() {
        taskTitleTextField.text = task?.title
        taskDescriptionTextView.text = task?.taskDescription
        taskDateTextField.text = task?.date
        taskStatusTextField.text = task?.status

        if let addButton = navigationItem.rightBarButtonItem, isTaskEditing {
            addButton.isEnabled = true
        }
        
    }
    
    func setupValidators() {
        let titleValid = taskTitleTextField.rx.text.orEmpty.map { $0.count >= minimalPasswordLength }
        let dateValid = taskDateTextField.rx.text.orEmpty.map { $0.count >= minimalPasswordLength }
        let statusValid = taskStatusTextField.rx.text.orEmpty.map { $0.count >= minimalPasswordLength }
        
        let everythingValid = Observable.combineLatest(titleValid, dateValid, statusValid) { $0 && $1 && $2 }
            .share(replay: 1)
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(insertNewObject(_:)))
        everythingValid.bind(to: addButton.rx.isEnabled).disposed(by: disposeBag!)
        navigationItem.rightBarButtonItem = addButton

    }

    func setupDatePicker() {
        datePicker.datePickerMode = .date
        //taskDateTextField.inputAccessoryView = toolbar
        taskDateTextField.inputView = datePicker
        //ToolBar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        
        toolbar.setItems([doneButton, spaceButton, cancelButton], animated: false)
        
        taskDateTextField.inputAccessoryView = toolbar
        taskDateTextField.inputView = datePicker
    }
    
    func setupStatusPicker() {
        statusPicker.delegate = self
        statusPicker.dataSource = self
        taskStatusTextField.inputView = statusPicker
    }
    
    @objc func donedatePicker() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        taskDateTextField.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker() {
        self.view.endEditing(true)
    }
    
    @objc
    func insertNewObject(_ sender: Any) {
        //let task = OwnTask()
        let submitTask = OwnTask()

        if !isTaskEditing {
            submitTask.taskId = "\(Date().timeIntervalSince1970)"
        } else {
            submitTask.taskId = task?.taskId ?? "\(Date().timeIntervalSince1970)"
        }
        
        submitTask.title = taskTitleTextField.text ?? ""
        submitTask.taskDescription = taskDescriptionTextView.text ?? ""
        submitTask.date = taskDateTextField.text ?? ""
        submitTask.status = taskStatusTextField.text ?? ""

        delegate?.updateModel(model: submitTask)
        presenter?.addTaskToRealm(task: submitTask)
    }

    func showPrompt() {
        let alert = UIAlertController(title: "Success", message: "Task has been added.", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Done", style: .default, handler: { (_) in
            self.dismiss(animated: true, completion: nil)
            }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showUpdatePrompt() {
        let alert = UIAlertController(title: "Success", message: "Task has been updated.", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Done", style: .default, handler: { (_) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }

    func didSelectPicker(status: String) {
        taskStatusTextField.text = status
    }
    
}

extension AddTaskViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return TaskStatus.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        guard let recurrenceOption = TaskStatus(rawValue: row) else {
            fatalError("Unknown RecurrenceOption")
        }
        
        return recurrenceOption.description
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard let recurrenceOption = TaskStatus(rawValue: row) else {
            fatalError("Unknown RecurrenceOption")
        }
        didSelectPicker(status: recurrenceOption.description)
    }

}
