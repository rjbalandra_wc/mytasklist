//
//  AddTaskPresenter.swift
//  MyTaskList
//
//  Created by RJ Balandra on 08/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift

protocol AddTaskPresenter: class {
    var numberOfTasks: Int { get }
    func viewDidLoad()
    func setupObservables(completionHandler: @escaping (_ result: Bool) -> Void)
    func attachView(view: AddTaskView)
    func configureWithData(cell: TaskListTableViewCell, for data: OwnTask)
    func didSelectWithData(task: OwnTask)
    func addTaskToRealm(task: OwnTask)
}

class AddTaskPresenterImplementation: AddTaskPresenter {

    var realm: Realm!
    var view: AddTaskView?
    var disposeBag: DisposeBag?

    var numberOfTasks: Int {
        do {
            return realm.objects(OwnTask.self).count
        }
    }
    func viewDidLoad() {

    }

    func setupObservables(completionHandler: @escaping (Bool) -> Void) {
        let laps = realm.objects(OwnTask.self)
        Observable.changeset(from: laps)
            .subscribe(onNext: { _, changes in
                if let changes = changes {
                   
                    if changes.inserted.count > 0 {
                        self.view?.showPrompt()
                    } else if changes.updated.count > 0 {
                        self.view?.showUpdatePrompt()
                    }

                    completionHandler(true)
                } else {
                    completionHandler(false)
                }
            }).disposed(by: disposeBag!)
    }

    func attachView(view: AddTaskView) {
        self.view = view
    }

    func configureWithData(cell: TaskListTableViewCell, for data: OwnTask) {
        cell.display(task: data)
    }

    func didSelectWithData(task: OwnTask) {
        //view?.showDetails(for: data)
    }

    func addTaskToRealm(task: OwnTask) {
        do {
            try self.realm.write({
                self.realm.add(task, update: true)
            })
        } catch { }
      
    }

}
