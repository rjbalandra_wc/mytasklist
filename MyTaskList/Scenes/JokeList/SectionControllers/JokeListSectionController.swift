//
//  JokeListSectionController.swift
//  MyTaskList
//
//  Created by RJ Balandra on 09/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import Foundation
import IGListKit

protocol RemoveCellDelegate: class {
    func removeCellDidTapButton(_ cell: JokeListCollectionViewCell)
}

// MARK: - ListSectionController
class JokeListSectionController: ListBindingSectionController<Joke>, RemoveCellDelegate {
    var joke: Joke!
    weak var view: JokeListView?
    
    override init() {
        super.init()
        
    }
    
    override func numberOfItems() -> Int {
        return 1
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width, height: 55)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        
        let cell = collectionContext!.dequeueReusableCell(withNibName: "JokeListCollectionViewCell", bundle: nil, for: self, at: index)
        if let cell = cell as? JokeListCollectionViewCell {
            //presenter?.configureWithData(cell: cell, for: comic)//(cell: cell, forRow: index)
            view?.configureWithData(cell: cell, for: joke)
            cell.delegate = self
            
        }
        
        return cell

    }
    
    override func didUpdate(to object: Any) {
        //ComicList = object as?
        joke = object as? Joke
    }
    
    override func didSelectItem(at index: Int) {
        //presenter?.didSelectWithData(data: comic)
        view?.didSelectWithData(data: joke)
    }
    
    // MARK: RemoveCellDelegate
    func removeCellDidTapButton(_ cell: JokeListCollectionViewCell) {
        view?.removeSectionControllerWantsRemoved(self)
    }
}
