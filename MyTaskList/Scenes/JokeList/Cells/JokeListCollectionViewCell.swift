//
//  JokeListCollectionViewCell.swift
//  MyTaskList
//
//  Created by RJ Balandra on 09/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import Kingfisher
import UIKit

protocol JokeListCell {
    
    func display(_ comic: Joke)
    func onButton(_ button: UIButton)
    
}

class JokeListCollectionViewCell: UICollectionViewCell, JokeListCell {
    
    @IBOutlet private weak var imgThumb: UIImageView!
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var lblDesc: UILabel!
    
    weak var delegate: RemoveCellDelegate?
    
    func display(_ joke: Joke) {
        
        lblTitle.text = "\(joke.id)"
        lblDesc.text = joke.value.isEmpty ? "No joke available" : joke.value
        
        imgThumb.kf.setImage(with: URL.init(string: joke.iconUrl), options: [.transition(.fade(0.3))])
        //removeButton.addTarget(self, action: #selector(onButton(_:)), for: .touchUpInside)
        
    }
    
    @objc func onButton(_ button: UIButton) {
        delegate?.removeCellDidTapButton(self)
        
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        setNeedsLayout()
        layoutIfNeeded()
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var newFrame = layoutAttributes.frame
        // note: don't change the width
        newFrame.size.height = ceil(size.height)
        layoutAttributes.frame = newFrame
        return layoutAttributes
    }
}
