//
//  JokeListPresenter.swift
//  MyTaskList
//
//  Created by RJ Balandra on 09/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift

typealias ComicsCompletionHandler = (_ query: String) -> Void

protocol JokeListPresenter: class {
    var numberOfJokes: Int { get }
    func viewDidLoad()
    func getJokes() -> [Joke]
    func attachView(view: JokeListView)
    func configureWithData(cell: JokeListCollectionViewCell, for data: Joke)
    func didSelectWithData(menu: Menu)
    func searchFilter(searchField: UITextField, completionHandler: @escaping FetchCompletionHandler)
}

class JokeListPresenterImplementation: JokeListPresenter {
    
    var realm: Realm!
    var disposeBag: DisposeBag?
    var view: JokeListView?
    var gateway: APIGateway?
    var jokes: [Joke] = []
    
    var numberOfJokes: Int {
        do {
            return jokes.count
        }
    }
    
    func viewDidLoad() {
        self.view?.showLoadingView()
        gateway?.fetchComics(params: ["query": "Chuck Norris"], completionHandler: { (value, error) in
            DispatchQueue.main.async {
                
                if error != nil {
                    //self.handleBooksError(fetchError)
                } else {
                    //self.handleBooksReceived(value)
                    self.jokes = value
                    self.view?.refreshJokeList()
                }
            }
        })
    }
    
    func getJokes() -> [Joke] {
        return jokes
    }
    func attachView(view: JokeListView) {
        self.view = view
    }
    
    func configureWithData(cell: JokeListCollectionViewCell, for data: Joke) {
        cell.display(data)
    }
    
    func didSelectWithData(menu: Menu) {
        //view?.showDetails(for: menu.segueIdentifier)
    }
    
    // MARK: Search Filter Handler
    func searchFilter(searchField: UITextField, completionHandler: @escaping FetchCompletionHandler) {
        
        searchField
            .rx.text // Observable property thanks to RxCocoa
            .orEmpty // Make it non-optional
            .debounce(3, scheduler: MainScheduler.instance) // Wait 0.5 for changes.
            .distinctUntilChanged()
            .filter { !$0.isEmpty }
            .subscribe(onNext: { query in
                self.view?.showLoadingView()
                self.gateway?.fetchComics(params: ["query": query], completionHandler: { (value, error) in
                    DispatchQueue.main.async {
                        
                        if error != nil {
                            //self.handleBooksError(fetchError)
                        } else {
                            //self.handleBooksReceived(value)
                            self.jokes = value
                            self.view?.refreshJokeList()
                        }
                    }
                })
                
            }, onCompleted: {
                print("completed")
            })
            .disposed(by: self.disposeBag!)
    }
    
}
