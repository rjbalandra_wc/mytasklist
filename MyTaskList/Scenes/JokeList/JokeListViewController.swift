//
//  JokeListViewController.swift
//  MyTaskList
//
//  Created by RJ Balandra on 09/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import UIKit
import IGListKit

protocol JokeListView: class {
    
    func refreshJokeList()
    func showLoadingView()
    func didSelectWithData(data: Joke)
    func configureWithData(cell: JokeListCollectionViewCell, for data: Joke)
    func numberOfItems() -> Int
    func removeSectionControllerWantsRemoved(_ sectionController: JokeListSectionController)

}

class JokeListViewController: UIViewController {
    
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var jokeCollectionView: UICollectionView!
    
    var presenter: JokeListPresenter?
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self, workingRangeSize: 0)
    }()
    
    var layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.size.width
        layout.estimatedItemSize = CGSize(width: width, height: 10)
        return layout
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter?.attachView(view: self)
        presenter?.viewDidLoad()
        self.jokeCollectionView.register(UINib(nibName: "JokeListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "JokeListCollectionViewCell")
        
        jokeCollectionView.collectionViewLayout = layout
        adapter.collectionView = jokeCollectionView
        adapter.dataSource = self
        self.adapter.performUpdates(animated: true)
        
        presenter?.searchFilter(searchField: self.searchField, completionHandler: { _, _ in
            
            self.adapter.performUpdates(animated: true)
            
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension JokeListViewController: JokeListView {
    func refreshJokeList() {
        UIView.animate(withDuration: 1.0, delay: 1.2, options: .curveEaseOut, animations: {
        }, completion: { _ in
            self.jokeCollectionView.isHidden = false
            self.adapter.performUpdates(animated: true)
        })
    }

    func showLoadingView() {
        UIView.animate(withDuration: 1.0, delay: 1.2, options: .curveEaseOut, animations: {
        }, completion: { _ in
            self.jokeCollectionView.isHidden = true
        })

    }

    func didSelectWithData(data: Joke) {

    }
    
    func configureWithData(cell: JokeListCollectionViewCell, for data: Joke) {
         presenter?.configureWithData(cell: cell, for: data)
    }

    func numberOfItems() -> Int {
        return presenter?.numberOfJokes ?? 0
    }

    func removeSectionControllerWantsRemoved(_ sectionController: JokeListSectionController) {
        
    }
}

extension JokeListViewController: ListAdapterDataSource {
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        if let items = presenter?.getJokes() {
            return items
        }
        
        return []
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        if object is Joke {
            let comicSectionController = JokeListSectionController()
            comicSectionController.view = self
            return comicSectionController
            
        } else {
            return ListSectionController()
        }
        
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? { return nil }
    
//    func removeSectionControllerWantsRemoved(_ sectionController: ComicsSectionController) {
//
//        let section = adapter.section(for: sectionController)
//
//        guard let object = adapter.object(atSection: section) as? RxComic  else { return }
//        localComicsList = presenter?.removeComic(comic: object, comicList: localComicsList) ?? []
//        self.presenter?.setFilteredComics(filteredComics: localComicsList)
//        self.adapter.performUpdates(animated: true, completion: { _ in
//            self.presenter?.updateSavedComics(comic: object)
//        })
//    }
}
