//
//  MasterViewController.swift
//  MyTaskList
//
//  Created by RJ Balandra on 07/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import RealmSwift
import RxRealm
import RxSwift
import UIKit

protocol TaskView: class {
    func showDetails(for task: OwnTask)
    func updateDisplayModel()
}

protocol TaskListViewControllerDelegate: class {
   
}

class TaskListViewController: UIViewController, TaskView, TaskListViewControllerDelegate {

    @IBOutlet weak var taskTableView: UITableView!
    var detailViewController: DetailViewController?
    var objects = [Any]()
    var realm: Realm!
    var presenter: TaskListPresenter?
    var disposeBag: DisposeBag?
    
    var localTaskList: [OwnTask] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(insertNewObject(_:)))
        navigationItem.rightBarButtonItem = addButton
        taskTableView?.register(UINib(nibName: "TaskListViewCell", bundle: nil), forCellReuseIdentifier: "TaskListTableViewCell")
        presenter?.attachView(view: self)
        presenter?.setupObservables(completionHandler: { _ in
            self.taskTableView.reloadData()
        })
        presenter?.viewDidLoad()
               
    }
    
    func updateDisplayModel() {
        localTaskList = presenter?.savedTasksList.map { $0 } ?? []
    }

    @objc
    func insertNewObject(_ sender: Any) {
        performSegue(withIdentifier: "segueToAddTask", sender: self)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = taskTableView.indexPathForSelectedRow {
                let object = presenter?.savedTasksList[indexPath.row]
                let controller = segue.destination as! DetailViewController
                controller.detailItem = object
            }
        }
    }

    func showDetails(for task: OwnTask) {
        performSegue(withIdentifier: "showDetail", sender: self)
    }

}

// MARK: - Table View
extension TaskListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return localTaskList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskListTableViewCell", for: indexPath)
        as! TaskListTableViewCell

        let object = localTaskList[indexPath.row]
        presenter?.configureWithData(cell: cell, for: object)
        return cell
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = localTaskList[indexPath.row]
        presenter?.didSelectWithData(data: object)
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let object = localTaskList[indexPath.row]
            localTaskList.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            presenter?.removeTaskFromRealm(data: object)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }

}
