//
//  TaskListPresenter.swift
//  MyTaskList
//
//  Created by RJ Balandra on 08/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift

protocol TaskListPresenter: class {
    var numberOfTasks: Int { get }
    func viewDidLoad()
    func setupObservables(completionHandler: @escaping (_ result: Bool) -> Void)
    func attachView(view: TaskView)
    func configureWithData(cell: TaskListTableViewCell, for data: OwnTask)
    func didSelectWithData(data: OwnTask)
    func removeTaskFromRealm(data: OwnTask)
    func getTasks() -> [OwnTask]
    var savedTasksList: Results<OwnTask> { get }
}

class TaskListPresenterImplementation: TaskListPresenter {

    var realm: Realm!
    var gateway: APIGateway?
    var view: TaskView?
    var disposeBag: DisposeBag?

    var savedTasksList: Results<OwnTask> {
        do {
            return realm.objects(OwnTask.self)
        }
    }

    var numberOfTasks: Int {
        do {
            return realm.objects(OwnTask.self).count
        }
    }

    func viewDidLoad() {
        self.view?.updateDisplayModel()
    }

    func setupObservables(completionHandler: @escaping (Bool) -> Void) {
        let laps = realm.objects(OwnTask.self)
        Observable.changeset(from: laps)
            .subscribe(onNext: { _, _ in
                self.view?.updateDisplayModel()
                completionHandler(true)
            }).disposed(by: disposeBag!)
    }

    func attachView(view: TaskView) {
        self.view = view
    }
    
    func getTasks() -> [OwnTask] {
        return savedTasksList.map { $0 }
    }

    func configureWithData(cell: TaskListTableViewCell, for data: OwnTask) {
        cell.display(task: data)
    }

    func didSelectWithData(data: OwnTask) {
        view?.showDetails(for: data)
    }

    func removeTaskFromRealm(data: OwnTask) {
        do {
            try self.realm.write({
                self.realm.delete(data)
            })
        } catch { }
    }

}
