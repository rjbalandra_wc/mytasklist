//
//  APIGateway.swift
//  MyTaskList
//
//  Created by RJ Balandra on 08/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import Foundation
import RxSwift
import Moya
import Moya_ObjectMapper
import Result

//typealias DeleteComicsCompletionHandler = (_ result: Result<Response, MoyaError>) -> Void
typealias FetchCompletionHandler = (_ comics: [Joke], _ error: Error?) -> Void
//typealias UploadComicsCompletionHandler = (_ image: UIImage, _ url: URL, _ deleteHash: String, _ error: Error?) -> Void

protocol APIGateway {
    func fetchComics(params: [String: Any], completionHandler: @escaping FetchCompletionHandler)
}

class APIGatewayImplementation: APIGateway {
    let bag = DisposeBag()
    
    // MARK: - Provider
    let provider = MoyaProvider<Jokes>()

    func fetchComics(params: [String: Any], completionHandler: @escaping FetchCompletionHandler) {
        
        provider.rx.request(.search(params)).subscribe { event in
            switch event {
            case let .success(event):
                do {
                    // 4
                    let payload = try event.mapObject(JokePayload.self)

                    completionHandler(payload.result ?? [], nil)
                } catch {
                    completionHandler([], error)
                }

            case let .error(error):
                print(error)
                completionHandler([], error)
            }

            }.disposed(by: bag)
    }

//    let imgurProvider = MoyaProvider<Imgur>()
//    static let bag = DisposeBag()
//
//    init() { }
//
//    func deletePhoto(deleteHash: String, completionHandler: @escaping DeleteComicsCompletionHandler) {
//        // 2
//        imgurProvider.request(.delete(deleteHash)) { response in
//
//            print(response)
//            completionHandler(response)
//
//        }
//    }
//
//    func uploadPhoto(card: UIImage,
//                     progressUpdate: ((_ percent: Float) -> Void)? = nil,
//                     completionHandler: @escaping UploadComicsCompletionHandler
//        ) {
//
//        imgurProvider.request(.upload(card), callbackQueue: DispatchQueue.main,
//                              progress: { progress in
//                                progressUpdate?(Float(progress.progress))
//        },
//                              completion: { response in
//                                switch response {
//                                case .success(let result):
//                                    do {
//                                        let upload = try result.map(ImgurResponse<UploadResult>.self)
//
//                                        print("uploaded")
//                                        completionHandler(card, upload.data.link, upload.data.deletehash, nil)
//
//                                    } catch {
//                                        //self.presentError()
//                                        print("error")
//                                        completionHandler(card, URL.init(fileURLWithPath: ""), "", error)
//                                    }
//                                case .failure:
//                                    print("error")
//                                    completionHandler(card, URL.init(fileURLWithPath: ""), "", nil)
//                                }
//        })
//
//    }
//
//    func fetchComics(completionHandler: @escaping FetchComicsCompletionHandler) {
//        provider.rx.request(.comics).subscribe { event in
//
//            switch event {
//            case let .success(event):
//                do {
//                    // 4
//                    let comicpayload = try event.mapObject(ComicPayload.self)
//                    let comicresults = comicpayload.results
//
//                    completionHandler(comicresults ?? [], nil)
//                } catch {
//                    completionHandler([], error)
//                }
//
//            case let .error(error):
//                completionHandler([], error)
//            }
//
//            }.disposed(by: ComicsGatewayImplementation.bag)
//
//    }
    
}
