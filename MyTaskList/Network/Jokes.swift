//
//  Jokes.swift
//  MyTaskList
//
//  Created by RJ Balandra on 08/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import Moya

public enum Jokes {
    // 1
    static private let publicKey = "174c4113745ac891734e4756ed8efe3f"
    static private let privateKey = "9d1e457a6c681b0f49d0ae986a67f27030245de5"
    
    // 2
    case search([String: Any])
}

extension Jokes: TargetType {
    
    // 1
    public var baseURL: URL {
        return URL(string: "https://api.chucknorris.io/jokes")!
    }
    
    // 2
    public var path: String {
        switch self {
        case .search: return "/search"
        }
    }
    
    // 3
    public var method: Moya.Method {
        switch self {
        case .search: return .get
        }
    }
    
    // 4
    public var sampleData: Data {
        return Data()
    }
    
    // 5
    public var task: Task {
//        let authParams = ["apikey": Jokes.publicKey, "ts": timestamp, "hash": hash]
        
        switch self {
        case .search(let params):
            // 3
            return .requestParameters(
                parameters: params,
                encoding: URLEncoding.default)
        }
    }
    
    // 6
    public var headers: [String: String]? {
        return ["Content-Type": "application/json"]
    }
    
    // 7
    public var validationType: ValidationType {
        return .successCodes
    }
}
