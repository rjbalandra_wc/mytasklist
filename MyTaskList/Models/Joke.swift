//
//  JokePayload.swift
//  MyTaskList
//
//  Created by RJ Balandra on 08/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import IGListKit
import ObjectMapper
import ObjectMapper_Realm
import ObjectMapperAdditions
import RealmSwift

// MARK: Initializer and Properties
class Joke: Object, Mappable, ListDiffable {
  
    @objc dynamic var id = ""
    var category: [String]? = []
    @objc dynamic var iconUrl = ""
    @objc dynamic var url = ""
    @objc dynamic var value = ""
    
    // MARK: JSON
    required convenience init?(map: Map) {
        self.init()
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        
        id <- (map["id"])
        iconUrl <- (map["icon_url"], StringTransform())
        url <- map["url"]
        value <- map["value"]
        category <- (map["category"])
    }
    
    public func diffIdentifier() -> NSObjectProtocol {
        return self
    }
    
    public func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        return isEqual(object)
    }
    
}
