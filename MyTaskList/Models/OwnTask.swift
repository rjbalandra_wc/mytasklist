//
//  Task.swift
//  MyTaskList
//
//  Created by RJ Balandra on 07/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import ObjectMapper
import ObjectMapper_Realm
import ObjectMapperAdditions
import RealmSwift

// MARK: Initializer and Properties
class OwnTask: Object, Mappable {

    @objc dynamic var taskId = ""
    @objc dynamic var title = ""
    @objc dynamic var taskDescription = ""
    @objc dynamic var date = ""
    @objc dynamic var status = ""
    
    // MARK: JSON
    required convenience init?(map: Map) {
        self.init()
    }

    override class func primaryKey() -> String? {
        return "taskId"
    }

    func mapping(map: Map) {

//        comicId <- (map["id"])
//        title <- (map["title"], StringTransform())
//        comicDescription <- (map["description"], StringTransform())
//        comicDescription <- (map["description"], StringTransform())

    }
    
    func getInterval(date: String) -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/y"
        guard let taskDate = dateFormatter.date(from: date) else {
            fatalError("ERROR: Date conversion failed due to mismatched format.")
        }
        
        if let diffInDays = Calendar.current.dateComponents([.day], from: taskDate, to: Date.init()).day {
            return diffInDays
        }
        
        return 0
        
    }
    
    func getRemark(date: String) -> TaskRemarks {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        guard let taskDate = dateFormatter.date(from: date) else {
            fatalError("ERROR: Date conversion failed due to mismatched format.")
            //return
        }
        
        if let diffInDays = Calendar.current.dateComponents([.day], from: taskDate, to: Date.init()).day {
            
            switch diffInDays {
                
            case let difference where difference < 15: return TaskRemarks.onboard
            case let difference where difference >= 15: return TaskRemarks.ongoing
            default: return TaskRemarks.accomplished
                
            }

        }
        
        return TaskRemarks.stopped
    }
    
}
