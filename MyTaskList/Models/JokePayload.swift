//
//  JokePayload.swift
//  MyTaskList
//
//  Created by RJ Balandra on 08/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import ObjectMapper

struct JokePayload: Mappable {
    
    var result: [Joke]?
    
    init?(map: Map) { }
    
    // Mappable
    mutating func mapping(map: Map) {
        result <- map["result"]
    }
    
}
