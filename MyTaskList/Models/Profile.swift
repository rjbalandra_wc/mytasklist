//
//  Profile.swift
//  MyTaskList
//
//  Created by RJ Balandra on 14/11/2018.
//  Copyright © 2018 RJ Balandra. All rights reserved.
//

import Foundation

class Profile: NSObject, NSCoding {
    var id: Int = 0
    var name: String = ""
    var profileDescription: String = ""
    
    override init() {
        super.init()
    }
    
    init(id: Int, name: String, description: String) {
        self.id = id
        self.name = name
        self.profileDescription = description
        
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeInteger(forKey: "id")
        let name = aDecoder.decodeObject(forKey: "name") as! String
        let profileDescription = aDecoder.decodeObject(forKey: "profileDescription") as! String
        self.init(id: id, name: name, description: profileDescription)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(profileDescription, forKey: "profileDescription")
    }
}
